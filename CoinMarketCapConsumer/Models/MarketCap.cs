﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinMarketCapConsumer.Models
{
    public class MarketCap
    {
        public int cmc_Rank { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public string MarketCapValue { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
        public string PercentageGain { get; set; }
    }
}