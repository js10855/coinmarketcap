﻿using CoinMarketCapConsumer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Configuration;
using System.Globalization;

namespace CoinMarketCapConsumer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string data = "";            
            List<MarketCap> MarketCapList = new List<MarketCap>();
            try
            {
                data = makeAPICall();
                //using (StreamReader r = new StreamReader(Server.MapPath("~/App_Data/data.json")))
                //{
                //    data = r.ReadToEnd();
                //}

                JObject obj = JObject.Parse(data);
                
                foreach (var val in obj.SelectToken("data"))
                {
                   

                    MarketCap marketCap = new MarketCap();
                    marketCap.cmc_Rank = Convert.ToInt32(val.SelectToken("cmc_rank"));
                    marketCap.Name = val.SelectToken("name").ToString();
                    decimal marketCapValue = Convert.ToDecimal(val.SelectToken("quote.USD.market_cap"));
                    string mcVal = ToKMB(marketCapValue);

                    //marketCap.MarketCapValue= "$" + Math.Round(marketCapValue,2) ;
                    marketCap.MarketCapValue = "$" + mcVal;
                    decimal price = Convert.ToDecimal(val.SelectToken("quote.USD.price"));
                    marketCap.Price = "$" + Math.Round(price, 2);

                    var iconUrl = ConfigurationManager.AppSettings.Get("iconUrl");
                        //"https://s2.coinmarketcap.com/static/img/coins/32x32/";
                    var icon = val.SelectToken("id").ToString() + ".png";
                    iconUrl += icon;
                    //marketCap.Icon = "/Icons/1.png";
                    marketCap.Icon = iconUrl;
                    MarketCapList.Add(marketCap);                    
                }
                
            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message);
            }           
            return View(MarketCapList);
        }

        private string ToKMB(decimal num)
        {
            if (num > 999999999 || num < -999999999)
            {
                return num.ToString("0,,,.##B", CultureInfo.InvariantCulture);
            }
            else
            if (num > 999999 || num < -999999)
            {
                return num.ToString("0,,.##M", CultureInfo.InvariantCulture);
            }
            else
            if (num > 999 || num < -999)
            {
                return num.ToString("0,.##K", CultureInfo.InvariantCulture);
            }
            else
            {
                return num.ToString(CultureInfo.InvariantCulture);
            }
        }

        public string makeAPICall()
        {
            string API_KEY = ConfigurationManager.AppSettings.Get("api_key"); //"0571bec8-6417-486a-8779-b060d8413995";
            //var URL = new UriBuilder("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest");
            var URL = new UriBuilder(ConfigurationManager.AppSettings.Get("url"));

            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["start"] = ConfigurationManager.AppSettings.Get("start");  //"1";
            queryString["limit"] = ConfigurationManager.AppSettings.Get("limit"); //"5000";
            queryString["convert"] = ConfigurationManager.AppSettings.Get("convert");//"USD";

            URL.Query = queryString.ToString();

            var client = new WebClient();
            client.Headers.Add("X-CMC_PRO_API_KEY", API_KEY);
            client.Headers.Add("Accepts", "application/json");
            return client.DownloadString(URL.ToString());

        }

     

        
    }
}